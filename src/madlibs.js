import {
  FIELD_NAMES,
} from './constants';

import { getTextTemplates } from './helpers';


// Action types
// ----------------------------------------------------------------------------

export const SUBMIT_FIELD = 'MADLIBS.SUBMIT_FIELD';
export const RESET_FORM = 'MADLIBS.RESET_FORM';
export const EDIT_FORM = 'MADLIBS.EDIT_FORM';

// Initial state
// ----------------------------------------------------------------------------
export const INITIAL_STATE = {
  fieldOrder: [
    FIELD_NAMES.hometown,
    FIELD_NAMES.favoriteFood,
    FIELD_NAMES.loveToDo,
    FIELD_NAMES.music,
    FIELD_NAMES.messageIf,
    FIELD_NAMES.bar,
  ],
  fieldAnswers: {},
  essayText: '',
  showEssayForm: false,
  showEssayText: true,
};

export const initForm = {
  hometown: '',
  favFood: '',
  loveTo: '',
  messageMe: '',
};

// Reducer
// ----------------------------------------------------------------------------
export function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case RESET_FORM: {
      return {
        ...state,
        fieldAnswers: {},
        essayText: '',
        showEssayForm: false,
        showEssayText: true,
      };
    }
    case SUBMIT_FIELD: {
      const { fieldName, answer } = action.payload;
      if (answer === '') return state;
      const textTemplates = getTextTemplates(fieldName);
      const randomIndex = Math.floor(Math.random() * textTemplates.length);
      const answers = textTemplates[randomIndex].replace('$answer', answer);
      const essayText = `${state.essayText} ${answers}`;
      return {
        ...state,
        fieldAnswers: {
          ...state.fieldAnswers,
          [fieldName]: answer,
        },
        essayText,
      };
    }
    case EDIT_FORM: {
      const { essayText } = action.payload;
      return {
        ...state,
        showEssayForm: true,
        showEssayText: false,
        essayText,
      };
    }
    default:
      return state;
  }
}

// Action creators
// ----------------------------------------------------------------------------
export function submitField({ id, answer }) {
  return { type: SUBMIT_FIELD, payload: { fieldName: id, answer } };
}

export function resetForm() {
  return { type: RESET_FORM };
}

export function editForm(txt) {
  return { type: EDIT_FORM, payload: { essayText: txt } };
}
