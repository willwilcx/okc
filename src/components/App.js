import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Form from './form/form';
import Essay from './essay/essay';

require('./App.scss');

const App = ({ showEssayText }) => (
  <div className="match-area">
    <Form />
    {showEssayText ? <Essay /> : null}
  </div>
);

App.propTypes = {
  showEssayText: PropTypes.bool.isRequired,
};

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps)(App);
