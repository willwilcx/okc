import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { editForm } from '../../madlibs';

const Essay = () => {
  const essayText = useSelector((state) => state.essayText);
  const dispatch = useDispatch();
  let resetButton;
  if (essayText && essayText.length) {
    resetButton = (
      <button
        onClick={() => dispatch(editForm(essayText))}
      >
        Edit
      </button>
    );
  }
  return (
    <div>
      <fieldset>
        <h4>Your essay text</h4>
        <p>{essayText}</p>
        {resetButton}
      </fieldset>
    </div>
  );
};

export default Essay;
