import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { submitField, resetForm, initForm } from '../../madlibs';

const Form = () => {
  const dispatch = useDispatch();
  const rState = useSelector((state) => state);
  const { showEssayForm, essayText } = rState;
  const [form, setForm] = useState({ ...initForm });
  const onBlur = (e) => {
    const { name, value } = e.target;
    setForm({
      ...form,
      [name]: value,
    });
    dispatch(submitField({ id: e.target.name, answer: e.target.value }));
  };

  return (
    <form>
      {showEssayForm ? (
        <fieldset>
          <h4>Your Essay</h4>
          <textarea defaultValue={essayText} />
          <button
            onClick={() => dispatch(resetForm())}
          >
            Start over
          </button>
        </fieldset>
      ) : (
        <fieldset>
          <h4>About Me</h4>
          <label htmlFor="hometown">Where did you grow up?</label>
          <input
            type="text"
            name="hometown"
            onBlur={onBlur}
          />
          <label htmlFor="favFood">Whats your favorite food?</label>
          <input
            type="text"
            name="favoriteFood"
            onBlur={onBlur}
          />
          <label htmlFor="loveTo">What do you LOVE to do?</label>
          <input
            type="text"
            name="loveToDo"
            onBlur={onBlur}
          />
          <label htmlFor="messageMe">
            People should message you if they...
          </label>
          <input
            type="text"
            name="messageIf"
            onBlur={onBlur}
          />
        </fieldset>
      )}
    </form>
  );
};

export default Form;
