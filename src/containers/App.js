import { connect } from 'react-redux';

import App from '../components/App';

function mapStateToProps(state) {
  return state;
}

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(App);
